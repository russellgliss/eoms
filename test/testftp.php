<?php

	require_once( "server.php" );
	
	$host = FTP_HOST;
	$port = 21;
	$username = FTP_USER;
	$password = FTP_PASS;
	
	echo "Connecting to " . $host . ".....";
	flush();
	if ( $ftpConn = ftp_connect( $host, $port ) ) {
		echo "connected<br>";
		flush();
		echo "Attempting to login with username : " . $username . ".....";
		flush();
		if ( ftp_login( $ftpConn, $username, $password ) ) {
			echo "<br>Logged in.<br><br>FTP test successful";
			flush();
		} else {
			echo "Unable to login";
		}
		ftp_close( $ftpConn );
	} else {
		echo "Error connecting to FTP site";
	}


?>