<?php 
	session_start();
	
	require_once( "db.php" );
	require_once( "server.php" );

	if ( ENABLED == false ) {
		echo "System is disabled";
		exit();
	}

	if (( isset( $_SESSION["user"] ) ) && ( isset( $_REQUEST["action"] ) ) && ($_REQUEST["action"]=='logout')) {
		logoff();
	}

	if ( !isset( $_SESSION["user"] ) ) {
		if ( ( isset( $_REQUEST["username"] ) ) && ( isset( $_REQUEST["password"] ) )  ) {
			if (!(loginUser( $_REQUEST["username"], $_REQUEST["password"] ))) {
				$message = "Invalid username or password!";
			}
		}
	}
	
	$customer = "";
	if ( isset( $_GET["customer"] ) ) {
		$customer = $_GET["customer"];		
	}
	
	//$_SESSION["user"] = "russell";
?>
<html>
	<head>
		<title>CHEP - Emergency Order System</title>
		<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Ubuntu">
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
		<script type="text/javascript" src="eoms.js"></script>
		<style>
		.class1 A:link {color:white}
		.class1 A:visited {color:white}
		.class1 A:active {color:white}
		.class1 A:hover {color:white}
		
			body, td, input, p {
				margin : 0px;
				spacing : 0px;
				font-family : "Ubuntu";
				font-size : 14px;
			}
			h1 {
				font-family: "Ubuntu", serif;
				font-size : 40px;
				color : #FFFFFF;
		}
      .login {
        font-family: "Ubuntu", serif;
        font-size: 40px;
        #text-shadow: 4px 4px 4px #aaa;
      }
      .login td {
      }
    </style>  
    <script type="text/javascript" src="calendarDateInput.js">
		/***********************************************
		 * Jason's Date Input Calendar- By Jason Moon http://calendar.moonscript.com/dateinput.cfm
 		 * Script featured on and available at http://www.dynamicdrive.com
		 * Keep this notice intact for use.
		 ***********************************************/
		</script>  
		<script>
		$(document).ready(loadDropDowns());

		function loadDropDowns() {
			$.getJSON( "plant.php", function(jsondata) {	
				if (jsondata['Error_spcMsg']) {
					//console.log(jsondata['Error_spcMsg']);					
					return;
				} else {
					
					var a = document.getElementById("plant");
					a.innerHTML = "";
					a.options[a.options.length] = new Option( "---- Plant ----", "" );
					for ( var c = 0; c < jsondata.length; c++ ) {
						a.options[a.options.length] = new Option( jsondata[c]["plant"] + " " + jsondata[c]["name"], jsondata[c]["plant"] );
					}
				}
			});
			$.getJSON( "product.php", function(jsondata) {	
				if (jsondata['Error_spcMsg']) {
					//console.log(jsondata['Error_spcMsg']);					
					return;
				} else {
					
					var a = document.getElementById("product");
					a.innerHTML = "";
					a.options[a.options.length] = new Option( "---- Product ----", "" );
					for ( var c = 0; c < jsondata.length; c++ ) {
						a.options[a.options.length] = new Option( jsondata[c]["plant"] + " " + jsondata[c]["name"], jsondata[c]["plant"] );
					}
				}
			});
				
		}
		</script>
	</head>
	<body>
		<table width="100%" cellspacing="0" cellpadding="0" class="login" style="background-color : #0066CC;color:white;">
			<tr>
				<td width="200"><img width="198" height="54" alt="CHEP" src="images/chep.png" border="0" /></td>
				<td align="center" valign="middle"><h1>Emergency Order System</h1></td>
			</tr>
			<tr>
			<td id="welcome" colspan="2" class="class1">
				<?php 
					if ( isset( $_SESSION["user"] ) ) {
						echo "Welcome ".$_SESSION["user"]." | <a href='?action=logout' style='padding : 10px;'>Logout</a>";
					} else {
						echo "&nbsp;";
					}
				?>
				</td>
			</tr>
		</table>
			
			<?php 
						
			if (isset( $_SESSION["user"] )) {				
				/* if ( checkFilesForUpload() ) {
					echo "Importing files<br />";
					importUploadedFiles();
					echo "Files imported<br />";
				} */
				if ( isset( $_REQUEST["action"] ) ) {
					switch ( $_REQUEST["action"] ) {
						case "capture" :
							displayCaptureForm();
							break;
						case "history" :
							displayOrderHistory();
							break;
						default :
							displayCaptureForm();
					}	
				}	else {			
					displayCaptureForm();
				}
			} else {
				displayLogin();
			}
			
			?>
	</body>
</html>

<?php 
	
	function loginUser( $username = null, $password = null ) {
		$authenticated = false;
		$hasMailbox = false;
		if ( ( $username != null ) && ( $password != null ) ) {
			// If user added "@cheporder.com" to end of username, remove it
			if ( strpos( $username, "@cheporder.com" ) > 0 ) {				
				$username = substr( $username, 0, strpos( $username, "@cheporder.com" ) );
			}
			if ( @imap_open ( "{mail.cheporder.com/notls}", $username, $password, OP_SILENT ) ) {
	  		$loginname = $username;
				$username .= "@cheporder.com";
				
				$_SESSION["user"] = $loginname;
				return true;
			} else {
				return false;
			}			
		}	else {
			return false;
		}
	}

	function displayLogin( $message="" ) {
		?>
		<div style="width:300px;margin:0px auto;">
					<form method="POST" action="index.php">
						<table>
							<tr>
								<td>Username</td>
								<td><input name="username" type="text" maxlength="50" size="20" /></td>
							</tr>
							<tr>
								<td>Password</td>
								<td><input name="password" type="password" maxlength="50" size="20" /></td>
							</tr>
							<tr>
								<td colspan="2" align="center"><input type="submit" value="Login" /></td>
							</tr>
							<?php
								
								if ( $message != "" ) {
									echo "<tr><td colspan='2'>" . $message . "</td></tr>";
								}
							
							?>
						</table>
					</form>
		</div>
		<?php 
	}
	
	function displayCaptureForm() {	
		echo "<table align='center' width='100%'>\n";
		echo "<tr valign='top'><td width='25%'>";
		echo "<table align='center'>";
		echo "<tr><td><a href='history.php'>Past Orders</a></td></tr>";
		echo "<tr><td><a href='http://mbox.cheporder.com' target='_new'>Mailbox</a></td></tr>";
		echo "</table>";
		echo "</td><td align='center'>";
		if ( isset( $_GET["orderNumber"] ) ) {
			echo "<font color='#00FF00'><b>";
			echo "Your Emergency Order has been successfully entered.  Order Number : " . str_pad( $_GET["orderNumber"], 10, "0", STR_PAD_LEFT );
			echo "</b></font>";
			if ( isset( $_GET["error"] ) ) {
				switch ( $_GET["error"] ) {
					case "email" : echo "<br><font color='#f99020'><b>Order created, email not sent to plant</b></font>"; break;
				}
			}
		} else if ( isset( $_GET["error"] ) ) {
			echo "<font color='#FF0000'><b>";
			switch ( $_GET["error"] ) {
				case "ftp" : echo "The export file could not be transfered to Lean Logistics, please try capturing the order again"; break;
				case "mysql" : echo "There was an error saving the order into the database, please contact the administrator"; break;
				default : "Your emergency order creation failed due to an unknown error, please contact the administrator";
			}
			echo "</b></font>";			
		}
		
		
		echo "<form id='orderForm' name='orderForm' action='submit.php' method='post'>";
		echo "<table>";
		echo "	<tr valign='middle'><td>Customer</td><td>";
		echo "		<input id='customerNumber' name='customerNumber' type='text' size='10' maxlength='10' onBlue='getCustomer();'>\n";		
		echo "		<a href='#' onclick='javascript:customerSearch();'><img src='images/Harmony/srch_16.gif' width='16' border='0'></a>\n";
		echo "		<input id='updateCustomer' type='hidden' value='update' onclick='getCustomer();' >";
		//echo "<select id='customer' name='customer' onchange='getCustomer();'>\n";
		
		//echo "<option value=''>---- Loading ... ----</option>\n";
		//echo "</select>\n";
		//getCustomerList();
		echo "</td></tr>\n";
		echo "	<tr><td>Plant</td><td>";
		echo "<select id='plant' name='plant' onchange='getPlant();'>\n";
		echo "<option value=''>---- Loading... ----</option>";
		echo "</select>\n";
		//getPlantList();		
		echo "</td></tr>\n";
		echo "	<tr><td>Product</td><td>";
		//echo "<select id='product' name='product' onchange='getProduct();'>\n";
		//echo "<option value=''>---- Product ----</option>\n";
		getProductList();		
		echo "</td></tr>\n";
		echo "	<tr><td>Batch</td><td>";
		getBatchList();		
		echo "</td></tr>\n";
		echo "	<tr><td>Quantity</td><td>";
		echo "<input id='quantity' name='quantity' type='text' size='4' value='1' onblur='return checkQuantity( this )' />&nbsp;&nbsp;&nbsp;";
		echo "<input id='qtyTypeEach' name='qtyType' type='radio' value='each' onClick='checkTruck();' checked> Each &nbsp;<input id='qtyTypeTruck' name='qtyType' type='radio' value='truck' onClick='checkTruck();'>Truck";
		echo "</td></tr>\n";
		echo "<tr><td>Drop</td><td><input name='drop' type='radio' value='N' checked> No &nbsp;<input name='drop' type='radio' value='Y'> Yes</td></tr>";
		echo "<tr><td>Direction</td><td><input name='direction' type='radio' value='OB' checked> Outbound &nbsp;<input name='direction' type='radio' value='IB'> Inbound</td></tr>";
		echo "<tr><td>Equipment</td><td><input name='commodity' type='radio' value='van' checked> Van &nbsp;<input name='commodity' type='radio' value='flatbed'> Flatbed</td></tr>";
		echo "<tr><td>Customer Purchase Order</td><td><input name='purchaseOrder' type='text' size='20'></td></tr>";		
		echo "<tr><td>Delivery Date</td><td><script>DateInput('orderdate', true, 'YYYY-MM-DD')</script></td></tr>";
		echo "<tr><td>Start Time</td><td>" . timeDropDown("startTime") . " - End Time " . timeDropDown("endTime") . "</td><tr>\n";
		echo "<tr><td>Comments</td><td><textarea name='comment' cols='50' rows='2'></textarea></td></tr>";
		echo "<tr><td colspan='2' align='center'><input id='submitButton' type='button' value='Place order' onclick='checkForm();'></td></tr>";
		echo "</table>";
		echo "</form>";
		
		echo "</td><td width='25%'>";
		echo "<table bgcolor='#e5e4ff' width='100%' style='border : 1px solid black;'>\n";
		echo "<tr><th>Customer</th></tr>\n";
		echo "<tr><td><span id='customerName'></span></td></tr>\n";
		echo "<tr><td><span id='customerStreet'></span></td></tr>\n";
		echo "<tr><td><span id='customerDistrict'></span></td></tr>\n";
		echo "<tr><td><span id='customerCity'></span></td></tr>\n";
		echo "<tr><td><span id='customerRegion'></span></td></tr>\n";
		echo "<tr><td><span id='customerPostal'></span></td></tr>\n";
		echo "<tr><td><span id='customerPhone'></span></td></tr>\n";
		echo "<tr><td><span id='customerFAX'></span></td></tr>\n";
		echo "<tr><td><span id='haulierComment'></span></td></tr>\n";
		echo "</table>\n";
		echo "<br>\n";
		echo "<table bgcolor='#e5e4ff' width='100%' style='border : 1px solid black;'>\n";
		echo "<tr><th>Plant</th></tr>\n";
		echo "<tr><td><span id='plantName'></span></td></tr>\n";
		echo "<tr><td><span id='plantStreet'></span></td></tr>\n";
		echo "<tr><td><span id='plantCity'></span></td></tr>\n";
		echo "<tr><td><span id='plantState'></span></td></tr>\n";
		echo "<tr><td><span id='plantZip'></span></td></tr>\n";
		echo "<tr><td><span id='plantManager'></span></td></tr>\n";
		echo "<tr><td><span id='plantPhone'></span></td></tr>\n";
		echo "<tr><td><span id='plantFAX'></span></td></tr>\n";
		echo "<tr><td><span id='plantEmail'></span></td></tr>\n";
		echo "</table>\n";
		echo "</td></tr>";
		echo "</table>";
	}
	
	function timeDropDown( $name ) {
		$r = "<select id='" . $name . "Hour' name='" . $name . "Hour'>";
		for ( $c = 0; $c < 24; $c++ ) {
			$r .= "<option value=$c>" . str_pad($c, 2, '0', STR_PAD_LEFT) . "</option>";
		}
		$r .= "</select>";
		$r .= ":";
		/*$r .= "<select name='" . $name . "Min'>";
		for ( $c = 0; $c < 60; $c++ ) {
			$r .= "<option value='$c'>" . str_pad($c, 2, '0', STR_PAD_LEFT) . "</option>";
		}
		$r .= "</select>";*/
		$r .= "<select id='" . $name . "Min' name='" . $name . "Min'>";
		$r .= "<option value=0>00</option>";
		$r .= "<option value=15>15</option>";
		$r .= "<option value=30>30</option>";
		$r .= "<option value=45>45</option>";
		$r .= "</select>";
		return $r;		
	}
	
	function getCustomerList( ) {
		$query = "select * from `customer` order by `name`";
		$result = mysql_query( $query );		
		echo "<select id='customer' name='customer' onchange='getCustomer();'>\n";
		echo "<option value=''>---- Customer ----</option>\n";
		while ( $row = mysql_fetch_array( $result ) ) {
			echo "<option value='" . $row["customerNumber"] . "'>" . $row["customerNumber"] . " - " . $row["name"] . "</option>\n";
		}
		echo "</select>\n";
	}
	
	function getPlantList() {
		$query = "select * from `plant` order by `plant`";
		$result = mysql_query( $query );		
		echo "<select id='plant' name='plant' onchange='getPlant();'>\n";
		echo "<option value=''>---- Plant ----</option>";
		while ( $row = mysql_fetch_array( $result ) ) {
			echo "<option value='" . $row["plant"] . "'>" . $row["plant"]." - ".$row["name"] . "</option>\n";
		}
		echo "</select>\n";
	}
	
	function getProductList() {
		$query = "select * from `product` order by `number`";
		$result = mysql_query( $query );		
		echo "<select id='product' name='product' onchange='getProduct();'>\n";
		echo "<option value=''>---- Product ----</option>\n";
		while ( $row = mysql_fetch_array( $result ) ) {
			echo "<option value='" . $row["number"] . "'>" . $row["number"] . " - " . $row["description"] . "</option>\n";
		}
		echo "</select>\n";
	}
	
	function getBatchList() {
		echo "<select id='batch' name='batch'>\n";
		echo "<option value=''>---- Batch ----</option>\n";
		echo "</select>";
	}

	function checkFilesForUpload() {
		if ( ( file_exists( "upload/customer.txt" ) ) && ( file_exists( "upload/customer.txt" ) ) && ( file_exists( "upload/customer.txt" ) ) ) {
			return true;														
		} else {
			return false;
		}		
	}
	
	
	
	function logoff() {
		unset( $_SESSION["user"] );
		unset($_REQUEST["action"]);
	}
	
	
?>