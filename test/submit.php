<?php

	session_start();
	
	require_once( "db.php" );
	require_once( "server.php" );
	
	define( "TRUCK_LOAD", 570 );
	define( "VAN", "VN" );
	define( "FLATBED", "FB" );
		

	if ( isset( $_SESSION["user"] ) ) {
		$order = array();
		
		//print_r( $_POST );
		
		$order["userComment"] = $_POST["comment"];
		$order["depotComment"] = "";
		$order["haulierComment"] = "";
		$query = "select * from `comments` where `glid` = '" . $_POST["customerNumber"] . "'";
		
		$result = mysql_query( $query );
		if ( $row = mysql_fetch_array( $result ) ) {
			if ( is_array( $row ) ) {
				$order["depotComment"] = $row["depot"];
				$order["haulierComment"] = $row["haulier"];
			}
		}
		$order["plantEmail"] = "";
		$query = "select `email` from `plant` where `plant` = '" . $_POST["plant"] . "'";
		$result = mysql_query( $query );
		$row = mysql_fetch_array( $result );
		if ( is_array( $row ) ) {
			$order["plantEmail"] = $row["email"];
		}
		
		$query = "select * from `customer` where `customerNumber` = '" . $_POST["customerNumber"] . "'";
		$result = mysql_query( $query );
		$row = mysql_fetch_array( $result );

		$order["drop"] = $_POST["drop"];
		if ( $_POST["direction"] == "OB" ) {
			$order["direction"] = "OB";
		} else {
			$order["direction"] = "IB";
		}
		$order["orderPurchaseNumber"] = $_POST["purchaseOrder"];
		$order["deliveryStartDate"] = $_POST["orderdate"] . " " . $_POST["startTimeHour"] . ":" . $_POST["startTimeMin"];
		$order["deliveryEndDate"] = $_POST["orderdate"] . " " . $_POST["endTimeHour"] . ":" . $_POST["endTimeMin"];
		$order["deliveryLocationRef"] = $_POST["customerNumber"] . "-" . substr( $row["street"], 0, 4 ) . "-" . substr( $row["postal"], 0, 5 );
		$order["deliveryName"] = $row["name"];
		$order["deliveryAddress2"] = $row["street"];
		$order["deliveryCity"] = $row["city"];
		$order["deliveryState"] = $row["region"];
		$order["deliveryZip"] = $row["postal"];
		$order["deliveryCountry"] = $row["country"];
		
		$query = "select * from `plant` where `plant` = '" . $_POST["plant"] . "'";
		$result = mysql_query( $query );
		$row = mysql_fetch_array( $result );
		
		$order["originStartDate"] = date( "Y-m-d", strtotime( date("Y-m-d") ) ) . " 00:01"; 
		$order["originEndDate"] = date( "Y-m-d", strtotime( date("Y-m-d") ) ) . " 23:59";
		$order["originLocationRef"] = $_POST["plant"] . "-" . substr( $row["street"], 0, 4 ) . "-" . substr( $row["zip"], 0, 5 );
		$order["originName"] = $row["name"];
		//$order["originAddress1"] = $row["name2"];
		$order["originAddress2"] = $row["street"];
		$order["originCity"] = $row["city"];
		$order["originState"] = $row["state"];
		$order["originZip"] = $row["zip"];
		$order["originCountry"] = $row["country"];
		
		$query = "select * from `product` where `number` = '" . $_POST["product"] . "'";
		$result = mysql_query( $query );
		$row = mysql_fetch_array( $result );
		
		$order["productID"] = $_POST["product"];
		if ( $_POST["qtyType"] == "truck" ) {
			//$order["productQuantity"] = $_POST["quantity"] * TRUCK_LOAD;
			$order["productQuantity"] = TRUCK_LOAD;
		} else {
			$order["productQuantity"] = $_POST["quantity"];
		}
		$order["productBatch"] = $_POST["batch"];
		$order["productDescription"] = $row["description"];
		if ( $_POST["commodity"] == "van" ) {
			$order["productCommodity"] = VAN . $order["productBatch"] . $order["productID"];				
		} else {
			$order["productCommodity"] = FLATBED . $order["productBatch"] . $order["productID"];
		}
			
		$query = "insert into `orders` ( userComment, depotComment, haulierComment, plantEmail, plantCode, user, orderDrop, orderDirection, orderPurchaseNumber, originStartDate, originEndDate, originLocationRef, originName, originAddress1, originAddress2, originCity, originState, originZip, originCountry, " .
			"deliveryStartDate, deliveryEndDate, deliveryLocationRef, deliveryName, deliveryAddress2, deliveryCity, deliveryState, deliveryZip, deliveryCountry, productID, productQuantity, productDescription, productBatch, productCommodity ) values " .
			"( '" . addslashes( $order["userComment"] ) . "', '" . addslashes( $order["depotComment"] ) . "', '" . addslashes( $order["haulierComment"] ) . "', '" . $order["plantEmail"] . "', '" . $_POST["plant"] . "', '" . $_SESSION["user"] . "', '" . $order["drop"] . "', '" . $order["direction"] . "', '" . $order["orderPurchaseNumber"] . "', '" . $order["originStartDate"] . "', '" . $order["originEndDate"] . "', '" . $order["originLocationRef"] . "', '" . addslashes( $order["originName"] ) . "', '" . $order["originAddress1"] . "', '" . $order["originAddress2"] . "', '" . $order["originCity"] . "', '" . $order["originState"] . "', '" . $order["originZip"] . "', '" . $order["originCountry"] . "', " .
			"'" . $order["deliveryStartDate"] . "', '" . $order["deliveryEndDate"] . "', '" . $order["deliveryLocationRef"] . "', '" . addslashes( $order["deliveryName"] ) . "', '" . $order["deliveryAddress2"] . "', '" . $order["deliveryCity"] . "', '" . $order["deliveryState"] . "', '" . $order["deliveryZip"] . "', '" . $order["deliveryCountry"] . "', '" . $order["productID"] . "', '" . $order["productQuantity"] . "', '" . $order["productDescription"] . "', '" . $order["productBatch"] . "', '" . $order["productCommodity"] . "' )";
		//echo $query;
		$result = mysql_query( $query );
		$insertId = mysql_insert_id();
		
		if ( $insertId != 0 ) {

			if ( uploadOrder( $insertId ) ) {
				if ( emailPlant( $insertId ) ) {
					header("Location: index.php?orderNumber=" . $insertId );
				} else {
					header("Location: index.php?orderNumber=" . $insertId . "&error=email");
				}
			} else {
				header("Location: index.php?error=ftp");
			}
		} else {
			header("Location: index.php?error=mysql&msg=" . mysql_error() );
		}
	
	} else {
		header("Location: index.php");
	}

function uploadOrder( $orderNumber ) {
	if ( FTP ) {
		$host = FTP_HOST;
		$port = 21;
		$username = FTP_USER;
		$password = FTP_PASS;
		
		
		$xmlFile = file_get_contents( XMLFILE . "export.php?orderNumber=" . $orderNumber );
		//$xmlFile = file_get_contents( "http://test.cheporder.com/export.php?orderNumber=" . $orderNumber );
		file_put_contents( "orders/" . str_pad( $orderNumber, 10, "0", STR_PAD_LEFT ) . ".xml", $xmlFile );	
		
		if ( $ftpConn = ftp_connect( $host, $port ) ) {		
			if ( ftp_login( $ftpConn, $username, $password ) ) {
				if ( ftp_put( $ftpConn, str_pad( $orderNumber, 10, "0", STR_PAD_LEFT ) . ".xml", "orders/" . str_pad( $orderNumber, 10, "0", STR_PAD_LEFT ) . ".xml", FTP_BINARY ) ) {
					return true;
				} else {
					echo "Unable to put file on FTP server";
					return false;
				}						
			} else {
				echo "Unable to login to FTP site";
				return false;
			}
			ftp_close( $ftpConn );
		} else {
			echo "Error connecting to FTP site";
			return false;
		}
	} else {
		return true;
	}		
}

function emailPlant( $orderNumber ) {
	if ( EMAIL ) {	
		$query = "select * from `orders` where `orderNumber` = " . $orderNumber;
		$result = mysql_query( $query );
		$row = mysql_fetch_array( $result );
		
		if ( preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $row["plantEmail"] ) ) {
			list( $username, $domain ) = split( "@", $row["plantEmail"] );
			if ( !checkdnsrr( $domain, "MX" ) ) {
				return false;
			}
		} else {
			return false;
		}
	
		$to      = EMAIL_LIST . "," . $_SESSION["user"] . "@cheporder.com";
		$subject = "CHEP *TEST* Emergency Order Created - Plant " . $row["plantCode"] . " - " . str_pad( $row["orderNumber"], 10, "0", STR_PAD_LEFT );
		$message = "A CHEP emergency order has been created for your plant in the TEST system.  Please see the load details below.  NOTE: this is not an actual order, please ignore.";
		
		$message .= "\n\n";
		$message .= "Delivery : " . date("M/d/Y H:i", strtotime( $row["deliveryStartDate"] ) ) . " - " . date("M/d/Y H:i", strtotime( $row["deliveryEndDate"] ) ) . "\n\n";
		$message .= "Material : " . $row["productDescription"] . " (" . $row["productID"] . ") / Batch : " . $row["productBatch"] . " / Qty : " . $row["productQuantity"] . "\n\n";
		if ( $row["orderDirection"] == "IB" ) {		
			$message .= "Pick Location :\n";
			$message .= "  " . $row["originLocationRef"] . "\n";
			$message .= "  " . $row["originName"] . "\n";
			if ( $row["originAddress1"] != "" ) { $message .= "  " . $row["originAddress1"] . "\n"; }
			if ( $row["originAddress2"] != "" ) { $message .= "  " . $row["originAddress2"] . "\n"; }
			$message .= "  " . $row["originCity"] . "\n";
			$message .= "  " . $row["originState"] . "\n";
			$message .= "  " . $row["originZip"] . "\n";
			
			$message .= "Drop Location :\n";
			$message .= "  " . $row["deliveryLocationRef"] . "\n";
			$message .= "  " . $row["deliveryName"] . "\n";
			if ( $row["deliveryAddress1"] != "" ) { $message .= "  " . $row["deliveryAddress1"] . "\n"; }
			if ( $row["deliveryAddress2"] != "" ) { $message .= "  " . $row["deliveryAddress2"] . "\n"; }
			$message .= "  " . $row["deliveryCity"] . "\n";
			$message .= "  " . $row["deliveryState"] . "\n";
			$message .= "  " . $row["deliveryZip"] . "\n";
		} else {
			$message .= "Pick Location :\n";
			$message .= "  " . $row["deliveryLocationRef"] . "\n";
			$message .= "  " . $row["originName"] . "\n";
			if ( $row["deliveryAddress1"] != "" ) {
				$message .= "  " . $row["deliveryAddress1"] . "\n";
			}
			if ( $row["deliveryAddress2"] != "" ) {
				$message .= "  " . $row["deliveryAddress2"] . "\n";
			}
			$message .= "  " . $row["deliveryCity"] . "\n";
			$message .= "  " . $row["deliveryState"] . "\n";
			$message .= "  " . $row["deliveryZip"] . "\n";
				
			$message .= "Drop Location :\n";
			$message .= "  " . $row["originLocationRef"] . "\n";
			$message .= "  " . $row["originName"] . "\n";
			if ( $row["originAddress1"] != "" ) {
				$message .= "  " . $row["originAddress1"] . "\n";
			}
			if ( $row["originAddress2"] != "" ) {
				$message .= "  " . $row["originAddress2"] . "\n";
			}
			$message .= "  " . $row["originCity"] . "\n";
			$message .= "  " . $row["originState"] . "\n";
			$message .= "  " . $row["originZip"] . "\n";				
		}
	
		$message .= "\n";
		$message .= "Special Comments:\n";
		$message .= $row["userComment"] . "\n";
		$message .= "Depot Instructions:\n";
		$message .= $row["depotComment"] . "\n";
		$message .= "Haulier Instructions:\n";
		$message .= $row["haulierComment"] . "\n";
		
		$headers = 'From: admin@cheporder.com' . "\r\n" .
	    	'Reply-To: admin@cheporder.com' . "\r\n" .
			'X-Mailer: PHP/' . phpversion();
		return mail($to, $subject, $message, $headers);
	} else {
		return true;
	}
}

?>