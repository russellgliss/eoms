<?php
	
session_start();

require_once( "db.php" );

if ( isset( $_POST["action"] ) && ( $_POST["action"] == "orderCaptured")) {
	$updateQuery = "update `orders` set `orderStatus` = 'C' where `orderNumber` = " . $_REQUEST["orderNumber"];
	$updateResult = mysql_query( $updateQuery );
	
	header("Location: history.php");
}

if ( isset( $_REQUEST["orderNumber"] ) ) {
	$query = "select * from `orders` where `orderNumber` = " . $_REQUEST["orderNumber"];
	$result = mysql_query( $query );
	$row = mysql_fetch_array( $result );
	if ( ( $row["orderUser"] == "" ) || ( $row["orderUser"] == $_SESSION["user"] ) ) {
		$updateQuery = "update `orders` set `orderUser` = '" . $_SESSION["user"] . "' where `orderNumber` = " . $_REQUEST["orderNumber"];
		$updateResult = mysql_query( $updateQuery );
		
		displayOrder(  $_REQUEST["orderNumber"] );
		
			
		
	} else {
		echo "Someone is already working on this order";
	}
	
} else {
	echo "Invalid Order Number";
}

function displayOrder( $orderNumber ) {
	$query = "select * from `orders` where `orderNumber` = " . $orderNumber;
	$result = mysql_query( $query );
	$row = mysql_fetch_array( $result );
	
	echo "<table align='center'>";
	echo "<tr><td>Order Number</td><td>" . $row["orderNumber"] . "</td></tr>\n";
	echo "<tr><td>Order Date</td><td>" . $row["orderDate"] . "</td></tr>\n";
	echo "<tr><td>User Comment</td><td>" . $row["userComment"] . "</td></tr>\n";
	echo "<tr><td>Captured By</td><td>" . $row["user"] . "</td></tr>\n";

	echo "<tr><td>Drop</td><td>" . $row["orderDrop"] . "</td></tr>\n";
	echo "<tr><td>Direction</td><td>" . $row["orderDirection"] . "</td></tr>\n";
	echo "<tr><td>Purchase Number</td><td>" . $row["orderPurchaseNumber"] . "</td></tr>\n";
	echo "<tr><td>Origin Start Date</td><td>" . $row["originStartDate"] . "</td></tr>\n";
	echo "<tr><td>Origin End Date</td><td>" . $row["originEndDate"] . "</td></tr>\n";
	if ( $row["orderDirection"] == "OB" ) {
		echo "<tr><td>Origin Location Reference</td><td>" . $row["originLocationRef"] . "</td></tr>\n";
		echo "<tr><td>Origin Name</td><td>" . $row["originName"] . "</td></tr>\n";
		echo "<tr><td>Origin Address 1</td><td>" . $row["originAddress1"] . "</td></tr>\n";
		echo "<tr><td>Origin Address 2</td><td>" . $row["originAddress2"] . "</td></tr>\n";
		echo "<tr><td>Origin City</td><td>" . $row["originCity"] . "</td></tr>\n";
		echo "<tr><td>Origin State</td><td>" . $row["originState"] . "</td></tr>\n";
		echo "<tr><td>Origin Zip</td><td>" . $row["originZip"] . "</td></tr>\n";
		echo "<tr><td>Origin Country</td><td>" . $row["originCountry"] . "</td></tr>\n";
	} else {
		echo "<tr><td>Origin Location Reference</td><td>" . $row["deliveryLocationRef"] . "</td></tr>\n";
		echo "<tr><td>Origin Name</td><td>" . $row["deliveryName"] . "</td></tr>\n";
		echo "<tr><td>Origin Address 2</td><td>" . $row["deliveryAddress2"] . "</td></tr>\n";
		echo "<tr><td>Origin City</td><td>" . $row["deliveryCity"] . "</td></tr>\n";
		echo "<tr><td>Origin State</td><td>" . $row["deliveryState"] . "</td></tr>\n";
		echo "<tr><td>Origin Zip</td><td>" . $row["deliveryZip"] . "</td></tr>\n";
		echo "<tr><td>Origin Country</td><td>" . $row["deliveryCountry"] . "</td></tr>\n";
	}
	echo "<tr><td>Delivery Start Date</td><td>" . $row["deliveryStartDate"] . "</td></tr>\n";
	echo "<tr><td>Delivery End Date</td><td>" . $row["deliveryEndDate"] . "</td></tr>\n";
	if ( $row["orderDirection"] == "OB" ) {
		echo "<tr><td>Delivery Location Reference</td><td>" . $row["originLocationRef"] . "</td></tr>\n";
		echo "<tr><td>Delivery Name</td><td>" . $row["originName"] . "</td></tr>\n";
		echo "<tr><td>Delivery Address 1</td><td>" . $row["originAddress1"] . "</td></tr>\n";
		echo "<tr><td>Delivery Address 2</td><td>" . $row["originAddress2"] . "</td></tr>\n";
		echo "<tr><td>Delivery City</td><td>" . $row["originCity"] . "</td></tr>\n";
		echo "<tr><td>Delivery State</td><td>" . $row["originState"] . "</td></tr>\n";
		echo "<tr><td>Delivery Zip</td><td>" . $row["originZip"] . "</td></tr>\n";
		echo "<tr><td>Delivery Country</td><td>" . $row["originCountry"] . "</td></tr>\n";
	} else {
		echo "<tr><td>Delivery Location Reference</td><td>" . $row["deliveryLocationRef"] . "</td></tr>\n";
		echo "<tr><td>Delivery Name</td><td>" . $row["deliveryName"] . "</td></tr>\n";
		//echo "<tr><td>Delivery Address 1</td><td>" . $row["deliveryAddress1"] . "</td></tr>\n";
		echo "<tr><td>Delivery Address 2</td><td>" . $row["deliveryAddress2"] . "</td></tr>\n";
		echo "<tr><td>Delivery City</td><td>" . $row["deliveryCity"] . "</td></tr>\n";
		echo "<tr><td>Delivery State</td><td>" . $row["deliveryState"] . "</td></tr>\n";
		echo "<tr><td>Delivery Zip</td><td>" . $row["deliveryZip"] . "</td></tr>\n";
		echo "<tr><td>Delivery Country</td><td>" . $row["deliveryCountry"] . "</td></tr>\n";		
	}
	echo "<tr><td>Product ID</td><td>" . $row["productID"] . "</td></tr>\n";
	echo "<tr><td>Product Quantity</td><td>" . $row["productQuantity"] . "</td></tr>\n";
	echo "<tr><td>Product Description</td><td>" . $row["productDescription"] . "</td></tr>\n";
	echo "<tr><td>Product Batch</td><td>" . $row["productBatch"] . "</td></tr>\n";
	echo "<tr><td>Product Commodity</td><td>" . $row["productCommodity"] . "</td></tr>\n";
	echo "</table>";	
	
	echo "<center>";
	echo "<form method='POST'>";
	echo "<input name='orderNumber' type='hidden' value='" . $orderNumber . "'>";
	echo "<input name='action' type='hidden' value='orderCaptured'>";
	echo "<input type='submit' value='Order captured'>";
	echo "</form>";
	echo "</center>";
	
	
	
	
}
	
	
?>