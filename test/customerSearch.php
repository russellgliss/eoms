<?php

	session_start();

	require_once( "db.php" );
	//require_once( "server.php" );
	
	if ( !isset( $_SESSION["user"] ) ) {
		header( 'Location: index.php' ) ;
	}
	echo "<style>\n";
	echo ".results { \n";
	echo "	font-family : Lucinda; \n";
	echo "	font-size : 14px; \n";
	echo "	border : 0px; \n";
	//echo "	padding : 4px; \n";
	echo "	border-spacing : 0px; \n";
	echo "}\n";
	echo ".results th { background-color : #9393ff; color : #FFFFFF; } \n";
	//echo ".results tr { \n";
	echo ".results tr:nth-child(odd)    { background-color:#ECECEC; }\n";
	echo ".results tr:nth-child(even)    { background-color:#CECECE; }\n";
	echo "}\n";
	echo "</style>\n";
	echo "<script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js'></script>\n";
	echo "<script type='text/javascript' src='eoms.js'></script>\n";
	
	
	echo "<form>\n";
	echo "	<table width='100%'>\n";
	echo "		<tr><td>Name</td><td><input id='customerName' name='customerName' size='30'";
	if ( ( isset( $_REQUEST["customerName"] ) ) && ( $_REQUEST["customerName"] != "" ) ) { echo " value='" . $_REQUEST["customerName"] . "'"; }
	echo "></td><td>GLID</td><td><input id='customerGLID' name='customerGLID' size='10' maxlength='10'";
	if ( ( isset( $_REQUEST["customerGLID"] ) ) && ( $_REQUEST["customerGLID"] != "" ) ) { echo " value='" . $_REQUEST["customerGLID"] . "'"; }
	echo "></tr>\n";	
	echo "		<tr><td>City</td><td><input name='customerCity' size='15'";
	if ( ( isset( $_REQUEST["customerCity"] ) ) && ( $_REQUEST["customerCity"] != "" ) ) { echo " value='" . $_REQUEST["customerCity"] . "'";	}
	echo "></td><td>Phone</td><td><input name='customerPhone' size='10'";
	if ( ( isset( $_REQUEST["customerPhone"] ) ) && ( $_REQUEST["customerPhone"] != "" ) ) { echo " value='" . $_REQUEST["customerPhone"] . "'";	}
	echo "></td></tr>\n";
	echo "		<tr><td>State</td><td><input name='customerState' size='2'";
	if ( ( isset( $_REQUEST["customerState"] ) ) && ( $_REQUEST["customerState"] != "" ) ) { echo " value='" . $_REQUEST["customerState"] . "'"; }
	echo "></td><td>FAX</td><td><input name='customerFAX' size='10'";
	if ( ( isset( $_REQUEST["customerFAX"] ) ) && ( $_REQUEST["customerFAX"] != "" ) ) { echo " value='" . $_REQUEST["customerFAX"] . "'"; }
	echo "></td></tr>\n";
	echo "		<tr><td>Zip</td><td><input name='customerZip' size='5'";
	if ( ( isset( $_REQUEST["customerZip"] ) ) && ( $_REQUEST["customerZip"] != "" ) ) { echo " value='" . $_REQUEST["customerZip"] . "'"; }
	echo ">-<input name='customerZipExt' size='4'";
	if ( ( isset( $_REQUEST["customerZipExt"] ) ) && ( $_REQUEST["customerZipExt"] != "" ) ) { echo " value='" . $_REQUEST["customerZipExt"] . "'"; }
	echo "></td><td>District</td><td><input name='customerDistrict' size='10'";
	if ( ( isset( $_REQUEST["customerDistrict"] ) ) && ( $_REQUEST["customerDistrict"] != "" ) ) { echo " value='" . $_REQUEST["customerDistrict"] . "'"; }
	echo "></td></tr>\n";
	echo "		<tr><td colspan='4' align='center'><input id='search' name='Search' type='submit' value='Search'></td></tr>\n";
	echo "	</table>\n";
	echo "</form>\n";
	//echo "	<div id='customerSearchResults'>\n";
	//echo "		<a href='#' onclick='selectCustomer(\"100020339\");'>100020339</a>\n";	
	//echo "	</div>\n";	

	
	if ( isset( $_REQUEST["customerName"] ) ) {
		$query = "select * from `customer` where 1 = 1 ";		
		if ( $_REQUEST["customerName"] != "" ) {
			$query .= "and `name` like '" . $_REQUEST["customerName"] . "%'";
		}
		if ( $_REQUEST["customerGLID"] != "" ) {
			$query .= "and `customerNumber` like '" . $_REQUEST["customerGLID"] . "%'";
		}
		if ( $_REQUEST["customerCity"] != "" ) {
			$query .= "and `city` like '" . $_REQUEST["customerCity"] . "%'";
		}
		if ( $_REQUEST["customerState"] != "" ) {
			$query .= "and `region` = '" . strtoupper( $_REQUEST["customerState"] ) . "'";
		}
		if ( $_REQUEST["customerZip"] != "" ) {
			if ( $_REQUEST["customerZipExt"] != "" ) {
				$query .= "and `postal` like '" . strtoupper( $_REQUEST["customerZip"] ) . "%" . $_REQUEST["customerZipExt"] . "'";
			} else {		
				$query .= "and `postal` like '" . strtoupper( $_REQUEST["customerZip"] ) . "%'";
			}
		}
		if ( $_REQUEST["customerPhone"] != "" ) {
			$query .= "and `phone` like '" . strtoupper( $_REQUEST["customerPhone"] ) . "%'";
		}
		if ( $_REQUEST["customerFAX"] != "" ) {
			$query .= "and `fax` like '" . strtoupper( $_REQUEST["customerFAX"] ) . "%'";
		}
		if ( $_REQUEST["customerDistrict"] != "" ) {
			$query .= "and `district` like '" . strtoupper( $_REQUEST["customerDistrict"] ) . "%'";
		}
		
		$query .= " order by `name`";
		//echo $query;
	
		$result = mysql_query( $query );
		echo "<table class='results' width='100%'>\n";
		echo "	<tr><th>GLID - name</th><th>City</th><th>State</th><th>Zip</th></tr>\n";
		while ( $row = mysql_fetch_array( $result ) ) {
			echo "<tr>";
			echo "<td><a href='#' onclick='selectCustomer(\"" . $row["customerNumber"] . "\");' title='" . htmlspecialchars( $row["street"] ) . "'>" . $row["customerNumber"] . " - " . $row["name"] . "</a></td>";
			echo "<td>" . $row["city"] . "</td>";
			echo "<td>" . $row["region"] . "</td>";
			echo "<td>" . $row["postal"] . "</td>";
			echo "</tr>";				
		}
		echo "</table>\n";
	
	
	} else {
		?>
		<script>
		if ( opener.document.getElementById("customerNumber").value != "" ) {
			document.getElementById("customerGLID").value = opener.document.getElementById("customerNumber").value;
			document.getElementById("search").click();
		} else {
			document.getElementById("customerName").focus();
		}
		</script>
		<?php 		
	}
	
?>
