<?php
	
	session_start();
	
	require_once( "db.php" );
	
	if ( isset( $_REQUEST["orderNumber"] ) ) {
		exportXML( $_REQUEST["orderNumber"] );	
	}	
	
	function exportXML( $orderNumber ) {
		$query = "select * from `orders` where `orderNumber` = '" . $orderNumber . "'";
		$result = mysql_query( $query );
		$xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n";
		while ( $row = mysql_fetch_array( $result ) ) {
			$xml .= "<LeanXML batchref=\"" . date( "mdYhis", strtotime( $row["orderDate"] ) ) . "\" created=\"" . date( "m/d/Y h:i:s", strtotime( $row["orderDate"] ) ) . "\">\n";
			$xml .= "<Order TransactionPurpose=\"ADD/UPDATE\">\n";
			$xml .= "<ShipperRef>" . str_pad( $row["orderNumber"], 10, "0", STR_PAD_LEFT ) . "</ShipperRef>\n";
			$xml .= "<UniqueRef>" . str_pad( $row["orderNumber"], 10, "0", STR_PAD_LEFT ) . "</UniqueRef>\n";
			$xml .= "<Comments>" . substr(htmlspecialchars( $row["userComment"] . " *** " . $row["haulierComment"] ), 0, 1000 ) . "</Comments>\n";
			$xml .= "<OrderType>1</OrderType>\n";
			$xml .= "<Workflow>1</Workflow>\n";
			$xml .= "<MethodOfPayment>PP</MethodOfPayment>\n";
			$xml .= "<DirectionCategory>" . $row["orderDirection"] . "</DirectionCategory>\n";
			$xml .= "<UserAssignment>" . $row["user"] . "</UserAssignment>\n";
			$xml .= "<GroupAssignment>AUTO</GroupAssignment>\n";
			$xml .= "<TMSPlanningAbility>1</TMSPlanningAbility>\n";
			$xml .= "<SalesGroup>US10</SalesGroup>\n";
			$xml .= "<ReferenceNums>\n";
			$xml .= "<Reference type=\"customerpo\">" . $row["orderPurchaseNumber"] . "</Reference>\n";
			$xml .= "</ReferenceNums>\n";
			if ( $row["orderDrop"] == "Y" ) {
				$xml .= "<ServiceRequests>\n";
				$xml .= "<ServiceRequest RequestCode=\"DROP\" Quantity=\"1\" Required=\"Yes\" ApplyLevel=\"1\" />\n";
				$xml .= "</ServiceRequests>\n";
			}
			$xml .= "<OrderSchedules>\n";
			$xml .= "<OrderSchedule>\n";
			$xml .= "<ScheduleNum>1</ScheduleNum>\n";
			$xml .= "<Origin>\n";
			if ( $row["orderDirection"] == "OB" ) {
				$xml .= "<EstAvailableStartDate TimeZone=\"ZZZ\">" . date( "m/d/Y H:i:s", strtotime( $row["originStartDate"])) . "</EstAvailableStartDate>\n";
				$xml .= "<EstAvailableEndDate TimeZone=\"ZZZ\">" . date( "m/d/Y H:i:s", strtotime( $row["originEndDate"])) . "</EstAvailableEndDate>\n";
			} else {
				$xml .= "<EstAvailableStartDate TimeZone=\"ZZZ\">" . date( "m/d/Y H:i:s", strtotime( $row["deliveryStartDate"])) . "</EstAvailableStartDate>\n";
				$xml .= "<EstAvailableEndDate TimeZone=\"ZZZ\">" . date( "m/d/Y H:i:s", strtotime( $row["deliveryEndDate"])) . "</EstAvailableEndDate>\n";				
			}
			$xml .= "<Location>\n";
			if ( $row["orderDirection"] == "OB" ) {
				$xml .= "<LocationRef>" . $row["originLocationRef"] . "</LocationRef>\n";
				$xml .= "<Name>" . htmlspecialchars( $row["originName"] ) . "</Name>\n";
				$xml .= "<Address1>" . htmlspecialchars( $row["originAddress1"] ) . "</Address1>\n";
				$xml .= "<Address2>" . htmlspecialchars( $row["originAddress2"] ) . "</Address2>\n";
				$xml .= "<City>" . $row["originCity"] . "</City>\n";
				$xml .= "<State>" . $row["originState"] . "</State>\n";
				$xml .= "<Zip>" . $row["originZip"] . "</Zip>\n";
				$xml .= "<Country>" . $row["originCountry"] . "</Country>\n";
			} else {
				$xml .= "<LocationRef>" . $row["deliveryLocationRef"] . "</LocationRef>\n";
				$xml .= "<Name>" . htmlspecialchars( $row["deliveryName"] ) . "</Name>\n";
				$xml .= "<Address2>" . htmlspecialchars( $row["deliveryAddress2"] ) . "</Address2>\n";
				$xml .= "<City>" . $row["deliveryCity"] . "</City>\n";
				$xml .= "<State>" . $row["deliveryState"] . "</State>\n";
				$xml .= "<Zip>" . $row["deliveryZip"] . "</Zip>\n";
				$xml .= "<Country>" . $row["deliveryCountry"] . "</Country>\n";
			}
			$xml .= "</Location>\n";
			$xml .= "</Origin>\n";
			$xml .= "<Destination>\n";
			$xml .= "<ReqDeliveryStartDate TimeZone=\"ZZZ\">" . date( "m/d/Y H:i:s", strtotime( $row["deliveryStartDate"])) . "</ReqDeliveryStartDate>\n";
			$xml .= "<ReqDeliveryEndDate TimeZone=\"ZZZ\">" . date( "m/d/Y H:i:s", strtotime( $row["deliveryEndDate"])) . "</ReqDeliveryEndDate>\n";
			$xml .= "<Location>\n";
			if ( $row["orderDirection"] == "OB" ) {
				$xml .= "<LocationRef>" . $row["deliveryLocationRef"] . "</LocationRef>\n";
				$xml .= "<Name>" . htmlspecialchars( $row["deliveryName"] ) . "</Name>\n";
				$xml .= "<Address2>" . htmlspecialchars( $row["deliveryAddress2"] ) . "</Address2>\n";
				$xml .= "<City>" . $row["deliveryCity"] . "</City>\n";
				$xml .= "<State>" . $row["deliveryState"] . "</State>\n";
				$xml .= "<Zip>" . $row["deliveryZip"] . "</Zip>\n";
				$xml .= "<Country>" . $row["deliveryCountry"] . "</Country>\n";
			} else {
				$xml .= "<LocationRef>" . $row["originLocationRef"] . "</LocationRef>\n";
				$xml .= "<Name>" . htmlspecialchars( $row["originName"] ) . "</Name>\n";
				$xml .= "<Address1>" . htmlspecialchars( $row["originAddress1"] ) . "</Address1>\n";
				$xml .= "<Address2>" . htmlspecialchars( $row["originAddress2"] ) . "</Address2>\n";
				$xml .= "<City>" . $row["originCity"] . "</City>\n";
				$xml .= "<State>" . $row["originState"] . "</State>\n";
				$xml .= "<Zip>" . $row["originZip"] . "</Zip>\n";
				$xml .= "<Country>" . $row["originCountry"] . "</Country>\n";
			}
			$xml .= "</Location>\n";
			$xml .= "</Destination>\n";
			$xml .= "</OrderSchedule>\n";
			$xml .= "</OrderSchedules>\n";
			$xml .= "<OrderItems>\n";
			$xml .= "<OrderItem ScheduleNum=\"1\">\n";
			$xml .= "<ProductID>" . str_pad( $row["productID"], 18, "0", STR_PAD_LEFT ) . "</ProductID>\n";
			$xml .= "<LineNumber>000010</LineNumber>\n";
			$xml .= "<Description>" . $row["productBatch"] . "</Description>\n";
			$xml .= "<PackageQuantity>" . $row["productQuantity"] . "</PackageQuantity>\n";
			$xml .= "<PackageType>PCS</PackageType>\n";
			$xml .= "<Weight>1</Weight>\n";
			$xml .= "<Volume>1</Volume>\n";
			$xml .= "<Commodity>" . $row["productCommodity"] . "</Commodity>\n";
			$xml .= "</OrderItem>\n";
			$xml .= "</OrderItems>\n";
			$xml .= "</Order>\n";
			$xml .= "</LeanXML>";
		}
		echo $xml;
	}	
	
	
?>