<?php
	
	session_start();
	
	require_once( "db.php" );
	
	if ( !isset( $_SESSION["user"] ) ) {
		header( 'Location: index.php' ) ;
	}
	
	if ( !isset( $_REQUEST["plant"] ) ) {
		//header( 'Location: index.php' ) ;
		header('Content-Type: application/json; charset=iso-8859-1');
			
		$query = "select `plant`, `name` from `plant` order by `plant`";
		$result = mysql_query( $query );
		$r = array();
		while ( $row = mysql_fetch_array( $result ) ) {
			$r[] = $row;
		}
		
		print_r( json_encode( $r ) );
		
	} else {
		header('Content-Type: application/json; charset=iso-8859-1'); 
			
		$query = "select * from `plant` where `plant` = '" . $_REQUEST["plant"] . "'";
		$result = mysql_query( $query );
		$row = mysql_fetch_array( $result );
		if ( preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $row["email"] ) ) {
			list( $username, $domain ) = explode( "@", $row["email"] );
			if ( !checkdnsrr( $domain, "MX" ) ) {
				$row["email"] = "<b>BAD email address</b> - " . $row["email"];
			}
		} else {
			$row["email"] = "<b>BAD email address</b> - " . $row["email"];
		}
		
		print_r( json_encode( $row ) );
	}
	
?>