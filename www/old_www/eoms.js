function getCustomer() {
	var customerNumber = document.getElementById('customerNumber').value;
	if ( customerNumber == '' ) {
		document.getElementById('customerName').innerHTML = 'Enter a customer number in the field.';
		return false; 
	}
	document.getElementById('customerName').innerHTML = '<center><img src="http://www.google.com/ig/images/spinner.gif" border="0"></center>'; 
	document.getElementById('customerStreet').innerHTML = '';
	document.getElementById('customerDistrict').innerHTML = '';		
	document.getElementById('customerCity').innerHTML = '';
	document.getElementById('customerRegion').innerHTML = '';		
	document.getElementById('customerPostal').innerHTML = '';
	document.getElementById('customerPhone').innerHTML = '';		
	document.getElementById('customerFAX').innerHTML = '';
	document.getElementById('haulierComment').innerHTML = '';
	
	var url = "customer.php?customer=" + customerNumber;
	
	$.getJSON(url, function(jsondata) {	
		if (jsondata['Error_spcMsg']) {
			//console.log(jsondata['Error_spcMsg']);
			return;
		} else {
			document.getElementById('customerName').innerHTML = '';			
		}
		
		if ( jsondata['name'] != undefined ) {
			var name = jsondata['name'];
			var country = jsondata['country'];
			var city = jsondata['city'];
			var postal = jsondata['postal'];
			var region = jsondata['region'];
			var street = jsondata['street'];
			var phone = jsondata['phone'];
			var fax = jsondata['fax'];
			var district = jsondata['district'];
			var haulierComment = jsondata['depot'];
			
			document.getElementById('customerName').innerHTML = name;
			document.getElementById('customerStreet').innerHTML = street;		
			document.getElementById('customerDistrict').innerHTML = district;		
			document.getElementById('customerCity').innerHTML = city;
			document.getElementById('customerRegion').innerHTML = region;		
			document.getElementById('customerPostal').innerHTML = postal;
			document.getElementById('customerPhone').innerHTML = '<img src="images/Harmony/phone_16.gif" border="0">' + phone;		
			document.getElementById('customerFAX').innerHTML = '<img src="images/Harmony/fax_16.gif" border="0">' + fax;		
			document.getElementById('haulierComment').innerHTML = haulierComment;
		} else {
			document.getElementById('customerName').innerHTML = "<font color='#FF0000'>Invalid customer number</font>";	
		}
		//console.log('end populateCustomer');
	});
}

function checkQuantity( qty ) {
	if ( isNaN( qty.value ) ) {
		alert( 'Quantity needs to be a number' );
		qty.focus();
		return false;
	} else {		
		return true;
	}	
}

function getPlant() {
	var plantID = document.getElementById('plant').value;
	if ( plantID == '' ) { return false; }
	document.getElementById('plantName').innerHTML = '<center><img src="http://www.google.com/ig/images/spinner.gif" border="0"></center>'; 
	document.getElementById('plantStreet').innerHTML = '';		
	document.getElementById('plantCity').innerHTML = '';
	document.getElementById('plantState').innerHTML = '';		
	document.getElementById('plantZip').innerHTML = '';
	document.getElementById('plantManager').innerHTML = '';
	document.getElementById('plantPhone').innerHTML = '';
	document.getElementById('plantFAX').innerHTML = '';
	document.getElementById('plantEmail').innerHTML = '';
	
	var url = "plant.php?plant=" + plantID;
		
	$.getJSON(url, function(jsondata) {	
		if (jsondata['Error_spcMsg']) {
			console.log(jsondata['Error_spcMsg']);
			return;
		} else {
			document.getElementById('plantName').innerHTML = '';
			
		}
	
		var name = jsondata['name'];
		var street = jsondata['street'];
		var city = jsondata['city'];
		var state = jsondata['state'];
		var zip = jsondata['zip'];
		var manager = jsondata['manager'];
		var phone = jsondata['phone'];
		var fax = jsondata['fax'];
		var email = jsondata['email'];
		
		document.getElementById('plantName').innerHTML = name; 
		document.getElementById('plantStreet').innerHTML = street;		
		document.getElementById('plantCity').innerHTML = city;
		document.getElementById('plantState').innerHTML = state;		
		document.getElementById('plantZip').innerHTML = zip;
		document.getElementById('plantManager').innerHTML = manager;
		document.getElementById('plantPhone').innerHTML = '<img src="images/Harmony/phone_16.gif" border="0">' + phone;
		document.getElementById('plantFAX').innerHTML = '<img src="images/Harmony/fax_16.gif" border="0">' + fax;
		document.getElementById('plantEmail').innerHTML = email;
		//console.log('end populatePlant');
	});
}

function getProduct() {	
	var productNumber = document.getElementById('product').value;
	if ( productNumber == '' ) { return false; }
	document.getElementById('batch').options.length = 0;
	
	var url = "product.php?product=" + productNumber;
		
	$.getJSON(url, function(jsondata) {	
		if (jsondata['Error_spcMsg']) {
			console.log(jsondata['Error_spcMsg']);
			return;
		} else {
			document.getElementById('plantName').innerHTML = '';
			
		}
		var select = document.getElementById('batch');
		select.options[select.options.length] = new Option('---- Batch ----', '');
		for ( var c = 0; c < jsondata.length; c++ ) {
			//alert( jsondata[c]['batch'] );			
			select.options[select.options.length] = new Option(jsondata[c]['batch'], jsondata[c]['batch']);
		}
	
		//var batch = jsondata['batch'];
		
		//alert( batch );
		
		//console.log('end populateProductBatch');
	});
}

function checkForm() {
	document.getElementById('submitButton').disabled = true;
	if ( document.getElementById('customer').value == '' ) {
		alert('Please select a customer');
		document.getElementById('customer').focus();
		document.getElementById('submitButton').disabled = false;
		return false;
	}
	if ( document.getElementById('plant').value == '' ) {
		alert('Please select a plant');
		document.getElementById('plant').focus();
		document.getElementById('submitButton').disabled = false;
		return false;
	}
	if ( document.getElementById('product').value == '' ) {
		alert('Please select a product');
		document.getElementById('product').focus();
		document.getElementById('submitButton').disabled = false;
		return false;
	}
	if ( document.getElementById('batch').value == '' ) {
		alert('Please select a batch');
		document.getElementById('batch').focus();
		document.getElementById('submitButton').disabled = false;
		return false;
	}
	if ( document.getElementById('quantity').value == '' ) {
		alert('Please enter a quantity');
		document.getElementById('quantity').focus();
		document.getElementById('submitButton').disabled = false;
		return false;
	}
	if ( document.getElementById('quantity').value <= '0' ) {
		alert('Please enter a positive value for quantity');
		document.getElementById('quantity').focus();
		document.getElementById('submitButton').disabled = false;
		return false;
	}
	// check times
	var startHour = document.getElementById("startTimeHour").options[document.getElementById("startTimeHour").selectedIndex].value;
	var startMin = document.getElementById("startTimeMin").options[document.getElementById("startTimeMin").selectedIndex].value;
	var endHour = document.getElementById("endTimeHour").options[document.getElementById("endTimeHour").selectedIndex].value;
	var endMin = document.getElementById("endTimeMin").options[document.getElementById("endTimeMin").selectedIndex].value;
	
	var start = ( parseInt( startHour ) * 60 ) + parseInt( startMin );
	var end = ( parseInt( endHour ) * 60 ) + parseInt( endMin );
	
	// Time Window
	if ( end - start < 120 ) {
		alert("There needs to be at least 2 hours difference in start and end times");
		document.getElementById('submitButton').disabled = false;
		return false;
	}
	//alert(document.orderForm.orderdate.value);
	
	var today = new Date();
	var orderDate = new Date( document.orderForm.orderdate.value );
	//orderDate = orderDate - 120 * 60 * 1000;
	orderDate.setDate( orderDate.getDate() + 1 );
	if ( today > orderDate ) {
		alert("Cant order before today");
		document.getElementById('submitButton').disabled = false;
		return false;
	} 
	
	//return false;	
	if ( confirm("Are you sure that you want to submit this order?") ) {
		return true;
	} else {
		document.getElementById('submitButton').disabled = false;
		return false;
	}
}

function checkTruck() {
	if ( document.getElementById("qtyTypeTruck").checked ) {
		document.getElementById("quantity").value = "1";
		document.getElementById("quantity").disabled = true;		
	} else {
		document.getElementById("quantity").value = "1";
		document.getElementById("quantity").disabled = false;		
	
	}
	
}

function selectCustomer( customerNumber ) {
	opener.document.getElementById("customerNumber").value = customerNumber;
	opener.document.getElementById("customerNumber").focus();
	opener.document.getElementById("updateCustomer").click();
	window.close();	
}

function customerSearch() {
	custWindow = window.open( "customerSearch.php", "customerSearch", "height=350,width=650,left=450,top=250,location=false,scrollbars=1");
	if ( window.focus ) { custWindow.focus() };
	return false;
}
