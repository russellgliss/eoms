<?php
	
	session_start();
	
	require_once( "db.php" );
	
	if ( !isset( $_SESSION["user"] ) ) {
		header( 'Location: index.php' ) ;
	}
	
?>
<html>
	<head>
		<title>CHEP - Emergency Order System</title>
		<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Ubuntu">
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
		<script type="text/javascript" src="eoms.js"></script>
		<style>
		.class1 A:link {color:white}
		.class1 A:visited {color:white}
		.class1 A:active {color:white}
		.class1 A:hover {color:white}
		
			body, td, input, p {
				margin : 0px;
				spacing : 0px;
				font-family : "Ubuntu";
				font-size : 14px;
			}
			h1 {
				font-family: "Ubuntu", serif;
				font-size : 40px;
				color : #FFFFFF;
		}
      .login {
        font-family: "Ubuntu", serif;
        font-size: 40px;
        #text-shadow: 4px 4px 4px #aaa;
      }
      .login td {
      }
    </style>  
    <script type="text/javascript" src="calendarDateInput.js">
		/***********************************************
		 * Jason's Date Input Calendar- By Jason Moon http://calendar.moonscript.com/dateinput.cfm
 		 * Script featured on and available at http://www.dynamicdrive.com
		 * Keep this notice intact for use.
		 ***********************************************/
		</script>  
	</head>
	<body>
		<table width="100%" cellspacing="0" cellpadding="0" class="login" style="background-color : #0066CC;color:white;">
			<tr>
				<td width="200"><img width="198" height="54" alt="CHEP" src="images/chep.png" border="0" /></td>
				<td align="center" valign="middle"><h1>Emergency Order System</h1></td>
			</tr>
			<tr>
			<td id="welcome" colspan="2" class="class1">
				<?php 
					if ( isset( $_SESSION["user"] ) ) {
						echo "Welcome ".$_SESSION["user"]." | <a href='?action=logout' style='padding : 10px;'>Logout</a>";
					} else {
						echo "&nbsp;";
					}
				?>
				</td>
			</tr>
		</table>
		
		<a href='index.php'>Submit Emergency Order</a>
		<br />
		<?php
			$status = "AllOpen";
			if ( isset( $_REQUEST["status"] ) ) {
				$status = $_REQUEST["status"];
			}
			$allOpen = "";
			$myOpen = "";
			$allClosed = "";
			switch ( $status ) {
				case "AllOpen" : $allOpen = "selected"; break;
				case "MyOpen" : $myOpen = "selected"; break;
				case "AllClosed" : $allClosed = "selected"; break;
			} 		
		?>
		
		<center>
		<form id="ordersForm" name="ordersForm" action="" method="get">
			<select name="status" onchange="document.ordersForm.submit();">
				<option value="AllOpen" <?php echo $allOpen ?>>All uncaptured orders</option>
				<option value="MyOpen" <?php echo $myOpen ?>>My accepted orders</option>
				<option value="AllClosed" <?php echo $allClosed ?>>All orders</option>				
			</select>
		</form>
		</center>
		<?php 
		
			switch ( $status ) {
				case "AllOpen" : $query = "select * from `orders` where `orderStatus` = 'O' and `orderUser` is null order by `orderNumber` desc"; break;
				case "MyOpen" : $query = "select * from `orders` where `orderUser` = '" . $_SESSION["user"] . "' and `orderStatus` = 'O' order by `orderNumber` desc"; break;
				case "AllClosed" : $query = "select * from `orders` order by `orderNumber` desc"; break;
			}
			//echo $query;
		
			//$query = "select * from `orders` order by `orderNumber` desc";
			$result = mysql_query( $query );
			
			echo "<table align='center'>";
			echo "<tr><th>Order</th><th>Customer</th><th>Plant</th><th>Product</th><th>Quantity</th><th>Date</th></tr>";
			$c = 0;
			while ( $row = mysql_fetch_array( $result ) ) {
				if ( $c % 2 == 0 ) {
					echo "<tr class='even'>";
				} else {
					echo "<tr class='odd'>";
				}
				echo "<td align='right'><a href='export.php?orderNumber=" . $row["orderNumber"] . "' target='_new'>" . $row["orderNumber"] . "</a></td>\n";
				if ( $row["orderDirection"] == "OB" ) {				
					echo "<td>" . $row["deliveryLocationRef"] . " - " . $row["deliveryName"] . "</td>\n";
					echo "<td title=\"" . $row["originLocationRef"] . "\">" . $row["originName"] . "</td>\n";
				} else {
					echo "<td>" . $row["originLocationRef"] . " - " . $row["originName"] . "</td>\n";
					echo "<td title=\"" . $row["deliveryName"] . "\">" . $row["deliveryLocationRef"] . "</td>\n";						
				}
				echo "<td>" . $row["productID"] . "-" . $row["productDescription"] . "</td>";
				echo "<td align='right'>" . $row["productQuantity"] . "</td>\n";
				echo "<td>" . $row["deliveryStartDate"] . " - " . $row["deliveryEndDate"] . "</td>\n";
				if ( $row["orderStatus"] == "O" ) {
					echo "<td><a href='capture.php?orderNumber=" . $row["orderNumber"] . "'>Capture</a></td>";
				} else {
					echo "<td>&nbsp;</td>";
				}				
				echo "</tr>";	
			}
			echo "</table>";
		
		?>		
	</body>
</html>