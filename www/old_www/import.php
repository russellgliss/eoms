<?php

	set_time_limit( 0 );

	include( "db.php" );

	function checkFilesForUpload() {
		if ( ( file_exists( "upload/customer.txt" ) ) && ( file_exists( "upload/customer.txt" ) ) && ( file_exists( "upload/customer.txt" ) ) ) {
			return true;														
		} else {
			return false;
		}		
	}
	
	function importUploadedFiles() {
		rename( "upload/customer.txt", "upload/customer.rdy" );											
		
		$f = fopen( "upload/customer.rdy", "r" );
		while ( !feof( $f) ) {
			$a = fgetcsv( $f );
			$query = "replace into `customer` ( `number`, `name`, `address` ) values ( '" . $a[0] . "', '" . $a[1] . "', '" . $a[2] . "' );";
			echo $query . "<br />";
			$result = mysql_query( $query );			
		}
		fclose( $f );
		
		unlink( "upload/customer.rdy" );
	}
	
	// Customer
	// Customer,Country,Name,City,Postal Code,Region,Street,Telephone 1,Fax Number,District
	
	$query = "DROP TABLE IF EXISTS `temp_customer`;";
	$result = mysql_query( $query );
	
	$query = "CREATE TABLE IF NOT EXISTS `temp_customer` ( " .
  	"`customerNumber` varchar(50) NOT NULL, " .
  	"`country` varchar(250) NOT NULL, " .
  	"`name` varchar(250) NOT NULL, " .
  	"`city` varchar(250) NOT NULL, " .
  	"`postal` varchar(250) NOT NULL, " .
  	"`region` varchar(250) NOT NULL, " .
  	"`street` varchar(250) NOT NULL, " .
  	"`phone` varchar(100) NOT NULL, " .
  	"`fax` varchar(100) NOT NULL, " .
  	"`district` varchar(250) NOT NULL, " .
  	"PRIMARY KEY (`customerNumber`) " .
		") ENGINE=MyISAM DEFAULT CHARSET=utf8;";
	$result = mysql_query( $query );		
	
	$row = 1;
	//$query = "insert into `temp_customer` ( customerNumber, country, name, city, postal, region, street, phone, fax, district ) \nvalues\n (\n";
	if ( ( $handle = fopen( "upload/customer.csv", "r" ) ) !== FALSE ) {
		$header = fgetcsv( $handle, 0, ",", "\"" );
    while ( ( $data = fgetcsv( $handle, 0, ",", "\"" ) ) !== FALSE) {
    	$query = "insert into `temp_customer` ( customerNumber, country, name, city, postal, region, street, phone, fax, district ) values ";
    	$query .= " ('" . $data[0] . "', '" . $data[1] . "', '" . addslashes( $data[2] ) . "', '" . $data[3] . "', '" . $data[4] . "', '" . $data[5] . "', '" . $data[6] . "', '" . $data[7] . "', '" . $data[8] . "', '" . $data[9] . "' )";
    	if ( $result = mysql_query( $query ) ) {
    		echo $data[0] . " - OK<br>";
    	} else {
    		echo $data[0] . " - ERROR : " . $query . "<br>";
    	}
		}
		fclose($handle);		
	}
	
	// Plants
	$query = "DROP TABLE IF EXISTS `temp_plant`;";
	$result = mysql_query( $query );
	
	$query = "CREATE TABLE IF NOT EXISTS `temp_plant` ( " .
  	"`plant` varchar(5) NOT NULL DEFAULT '', " .
  	"`name1` varchar(30) DEFAULT NULL, " .
  	"`cutomerNoPlant` varchar(15) DEFAULT NULL, " .
  	"`factoryCalendar` varchar(15) DEFAULT NULL, " .
  	"`name2` varchar(30) DEFAULT NULL, " .
  	"`street` varchar(30) DEFAULT NULL, " .
  	"`pobox` varchar(17) DEFAULT NULL, " .
  	"`postal` varchar(11) DEFAULT NULL, " .
  	"`city` varchar(22) DEFAULT NULL, " .
  	"`country` varchar(7) DEFAULT NULL, " .
  	"`region` varchar(12) DEFAULT NULL, " .
  	"PRIMARY KEY (`plant`) " .
		") ENGINE=MyISAM DEFAULT CHARSET=utf8;";
	$result = mysql_query( $query );

	$row = 1;
	if ( ( $handle = fopen( "upload/plant.csv", "r" ) ) !== FALSE ) {
		$header = fgetcsv( $handle, 0, ",", "\"" );
    while ( ( $data = fgetcsv( $handle, 0, ",", "\"" ) ) !== FALSE) {
    	$query = "insert into `temp_plant` ( plant, name1, cutomerNoPlant, factoryCalendar, name2, street, pobox, postal, city, country, region  ) values ";
    	$query .= " ('" . $data[0] . "', '" . addslashes( $data[1] ) . "', '" . $data[2] . "', '" . $data[3] . "', '" . addslashes( $data[4] ) . "', '" . addslashes( $data[5] ) . "', '" . addslashes( $data[6] ) . "', '" . $data[7] . "', '" . addslashes( $data[8] ) . "', '" . $data[9] . "', '" . $data[10] . "' )";    	
    	if ( $result = mysql_query( $query ) ) {
    		echo $data[0] . " - OK<br>";
    	} else {
    		echo $data[0] . " - ERROR : " . $query . "<br>";
    	}
		}
		fclose($handle);		
	}
	
	// Product
	$query = "DROP TABLE IF EXISTS `temp_product`;";
	$result = mysql_query( $query );
	
	$query = "CREATE TABLE IF NOT EXISTS `temp_product` ( " .
  	"`number` varchar(10) NOT NULL, " .
  	"`description` varchar(250) DEFAULT NULL, " .
  	"PRIMARY KEY (`number`) " .
		") ENGINE=MyISAM DEFAULT CHARSET=utf8;";
	$result = mysql_query( $query );

	$row = 1;
	if ( ( $handle = fopen( "upload/product.csv", "r" ) ) !== FALSE ) {
		$header = fgetcsv( $handle, 0, ",", "\"" );
    while ( ( $data = fgetcsv( $handle, 0, ",", "\"" ) ) !== FALSE) {
    	$query = "insert into `temp_product` ( number, description  ) values ";
    	$query .= " ('" . $data[0] . "', '" . addslashes( $data[1] ) . "' )";
    	if ( $result = mysql_query( $query ) ) {
    		echo $data[0] . " - OK<br>";
    	} else {
    		echo $data[0] . " - ERROR : " . $query . "<br>";
    	}
		}
		fclose($handle);		
	}
	
	// Batch
	$query = "DROP TABLE IF EXISTS `temp_batch`;";
	$result = mysql_query( $query );
	
	$query = "CREATE TABLE IF NOT EXISTS `temp_batch` ( " .
  	"`product` varchar(10) NOT NULL, " .
  	"`batch` varchar(100) NOT NULL, " .
  	"PRIMARY KEY (`product`,`batch`) " .
		") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
	$result = mysql_query( $query );

	$row = 1;
	if ( ( $handle = fopen( "upload/batch.csv", "r" ) ) !== FALSE ) {
		$header = fgetcsv( $handle, 0, ",", "\"" );
    while ( ( $data = fgetcsv( $handle, 0, ",", "\"" ) ) !== FALSE) {
    	$query = "insert into `temp_batch` ( product, batch  ) values ";
    	$query .= " ('" . $data[0] . "', '" . $data[1] . "' )";
    	if ( $result = mysql_query( $query ) ) {
    		echo $data[0] . " - OK<br>";
    	} else {
    		echo $data[0] . " - ERROR : " . $query . "<br>";
    	}
		}
		fclose($handle);		
	}
	
	// RENAME TABLE old_table TO backup_table




?>