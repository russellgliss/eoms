<?php

	require_once( "db_master.php" );
	
	define("GREEN_TICK", "<img src='images/Harmony/up_16.gif' border='0'>");
	define("RED_CROSS", "<img src='images/Harmony/close_16.gif' border='0'>");
	
	if ( isset( $_REQUEST["action"] ) ) {
		switch ( $_REQUEST["action"] ) {
			case "Import" : importFiles(); break;
			case "Confirm" : confirmImport(); break;
		}		
	} else {		
		if ( file_exists( "import.ini" ) ) {
			$importFiles = parse_ini_file( "import.ini", true );
			
			//print_r( $importFiles );
			
			$importReady = true;
			echo "<table>";
			foreach ( $importFiles as $file ) {
				echo "<tr>";
				echo "<td >" . $file["description"] . "</td>";
				if ( file_exists( $file["filename"] ) ) {
					echo "<td>" . GREEN_TICK . "</td>";
					if ( $f = fopen( $file["filename"], "rt" ) ) {
						$fstat = fstat( $f );
						$d = getDate( $fstat["mtime"] );
						echo "<td>Date - " . $d["year"] . "/" . $d["mon"] . "/" . $d["mday"] . " " . $d["hours"] . ":" . $d["minutes"] . ":" . $d["seconds"] . "</td>";
						echo "<td>Size - " . $fstat["size"] . "</td>";
						if ( $fstat["size"] == 0 ) {
							$importReady = false;
						} else {
							if ( $lines = file( $file["filename"] ) ) {
								echo "<td>Lines - " . sizeof( $lines ); 
							} else {
								echo "<td>Unable to detemine line count";
								$importReady = false;
							}
							
						}
					} else {
						echo "<td colspan='2'>Unable to open file</td>";
						$importReady = false;
					}
					
				} else {
					echo "<td>" . RED_CROSS . "&nbsp;Unable to find file " . $file["filename"] . "</td>";
					$importReady = false;
				}			
				echo "</tr>";
			}
			echo "</table>";
			
			if ( $importReady ) {
				echo "<form method='get'>";		
				echo "<input name='action' type='submit' value='Import'>";
				echo "</form>";
			}
			
		} else {
			echo "File missing : import.ini.  Unable to continue!";
		}
	}
	
	function importFiles() {
		echo "Importing...<br>";
		if ( file_exists( "import.ini" ) ) {
			$importFiles = parse_ini_file( "import.ini", true );
			
			//print_r( $importFiles );
			
			
			foreach ( $importFiles as $file ) {
				if ( $file["type"] == "csv" ) {
					$fileHandle = fopen( $file["filename"], "rt" );
					echo "File : " . $file["filename"] . "<br>";
					$line = array();
					while ( !feof( $fileHandle ) ) {
						$line[] = fgetcsv( $fileHandle );
					}
					
					$query = "show create table `" . $file["dbTable"] . "`";
					//echo $query . "<br>";
					if ( $result = mysql_query( $query ) ) {
						
					} else {
						echo "Could not execute query : " . $query . "<br>Error : " . mysql_error();
						break;
					}
					$row = mysql_fetch_array( $result );
					//echo $row[1];
					
					$query = "DROP TABLE IF EXISTS `temp_" . $file["dbTable"] . "`";
					if ( $result = mysql_query( $query ) ) {
						echo "Temp table `temp_" . $file["dbTable"] . "` dropped<br>";
					} else {
						echo "Error, could not drop `temp_" . $file["dbTable"] . "` : " . mysql_error() . "<br>";
					}
					
										
					$tempTable = str_replace( "CREATE TABLE `" . $file["dbTable"] . "`", "CREATE TABLE `temp_" . $file["dbTable"] . "`", $row[1] );
					//$tempTable = "DROP TABLE IF EXISTS `temp_" . $file["dbTable"] . "`; " . $tempTable;
					//echo $tempTable . "<br>";
					$result = mysql_query( $tempTable );
					
					$cnt = 0;
					$errCnt = 0;
					for ( $c = $file["headerLines"]; $c < sizeof( $line ) - $file["footerLines"] - 1; $c++ ) {
						$query = "insert into `temp_" . $file["dbTable"] . "` ( " . $file["dbFields"] . " ) " .
							"values ( ";
						$valueArray = explode( ",", $file["dbColumns"] );
						//print_r( $line[$c] );
						
						foreach ( $valueArray as $value ) {
							//echo $value . ", ";
							if ( $value != "" ) {
								$query .= "'" . addslashes( htmlspecialchars( $line[$c][$value] ) ) . "', ";
							} else {
								$query .= "'', ";
							}							
						}
						$query = substr( $query, 0, strlen($query) - 2) . " )";
						//echo "<br>" . $query . "<br>";
					
						if ( $result = mysql_query( $query ) ) {
							$cnt++;
						} else {
							echo "Error with line : " . $c . ", " . mysql_error() . "<br>" . $query . "<br>";
							$errCnt++;
						}
					}
					echo "Imported : " . $cnt . "<br>";
					echo "Error : " . $errCnt . "<br>";
					echo "Rows in current table : ";
					$query = "select count(*) from `" . $file["dbTable"] . "`";
					$result = mysql_query( $query );
					$row = mysql_fetch_array( $result );
					echo $row[0] . "<br>";
					
					echo "<hr>";
					
					
				
				
					
				} else {
				/*
					$lines = file( $file["filename"] );
					echo "Lines : " . sizeof( $lines );
					//for ( $c = $file["headerLines"]; $c < sizeof( $lines ) - $file["footerLines"]; $c++ ) {
					for ( $c = $file["headerLines"]; $c < 5; $c++ ) {
						$line = $lines[$c];
						if ( preg_match_all( "/" . $file["regex"] . "/", $line, $data ) !== FALSE ) {
							print_r( $data );
							
							foreach ( $data as $dataLine ) {
								
							}
							
						} else {
							echo "Error matching data pattern.";
						}
					}*/
				}		
			}
			echo "Done";
			
			echo "<form method='get'>";
			echo "<input name='action' type='hidden' value='Confirm'>";
			echo "<input name='submit' type='submit' value='Confirm Import'>";
			echo "</form>";
				
		} else {
			echo "No import.ini. Stopping!";
		}
	}
	
	function confirmImport() {
		if ( file_exists( "import.ini" ) ) {
			$importFiles = parse_ini_file( "import.ini", true );

			foreach ( $importFiles as $file ) {
				$query = "RENAME TABLE `" . $file["dbTable"] . "` TO `" . $file["dbTable"] . "_" . date( "YmdHis" ) . "`";
				$result = mysql_query( $query );
					
				$query = "RENAME TABLE `temp_" . $file["dbTable"] . "` TO `" . $file["dbTable"] . "`";
				$result = mysql_query( $query );				
			}
			echo "Import complete";
		}
	}
		
?>
	